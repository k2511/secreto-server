FROM golang:alpine

RUN apk update
RUN apk add git

WORKDIR /app
COPY . /app

RUN go mod download
RUN go build -o secreto-server .
RUN chmod +x secreto-server

EXPOSE 8080
CMD ["/app/secreto-server"]