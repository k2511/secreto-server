# Kube Secreto 🤐

Manage your Kubernetes Secrets🔒 with one simple API
made just for secrets. With Kube Secreto you'll be able to:

- Create a Generic Secret
- List Secrets by NameSpace
- View Secret Payload
- Delete Secrets

For more information checkout the [api documentation](docs/api.md) as well
as the [development guide](docs/development.md).

**Note:** This microservice has been made with learning
in mind. Please see the following blogs to learn more about
building applications with Kubernetes client-go.

- [Build, Test, and Automate a Kubernetes Interfacing Application in Go](https://awkwardferny.medium.com/build-test-and-automate-a-kubernetes-interfacing-application-in-go-da71e4d5aaef)
- [Adding a Minimalistic ReactJS UI to your Kubernetes Application](https://awkwardferny.medium.com/adding-a-minimalistic-reactjs-ui-to-your-kubernetes-application-d4e1859d312b)

The documentation includes reasons on why I designed the application this
way. I was inspired by [Shubham Chadokar's](https://medium.com/@schadokar) blog on [Build a Todo App in Golang, MongoDB, and React](https://levelup.gitconnected.com/build-a-todo-app-in-golang-mongodb-and-react-e1357b4690a6)
and just for fun the label used for the secret data (*shhh*) was inspired by
[It's Always Sunny in Philadelphia: The Gang Gets Shushed](https://youtu.be/CI22CMzL7oo?t=89)

## Deployment and Usage

You can deploy this application in different ways. There's out-of-cluster
for running the application on your local machine, as well as in-cluster
which allows you to deploy and access the application from Kubernetes.

### Requirements

- Kubernetes Cluster: You can create a local cluster using [minikube](https://minikube.sigs.k8s.io/docs/) or
use one of the several public offerings.
- Go: You must have GoLang installed in order to run this locally.

### Out-of-Cluster

In order to use this application locally, you must pass the `-local`
flag. This allows us to use our local KUBECONFIG for connecting to
the cluster.

```bash
$ make build

# Launch the Secreto Server
$ ./secreto-server -local
```

In order to change the port, set the **SECRETO_PORT** environment
variable to the desired port.

### In-Cluster

This application can be hosted within a cluster and accessed from
anywhere with the correct configuration. In order to do this, you
can use the helm chart I created [here](https://gitlab.com/k2511/secreto-helm).

**Disclaimer:** This should not be run in production since
secret data is exposed. In a production app, a login would
be required.

---

Project Avatar by <a href="https://unsplash.com/@tinaflour?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Kristina Flour</a> on <a href="https://unsplash.com/s/photos/shh?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
