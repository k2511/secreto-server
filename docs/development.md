# Development Guide

This development guide goes over how this application
works as well as what is required.

## Pre-requisites

- Kubernetes Cluster
    You can use [Minikube](https://minikube.sigs.k8s.io/docs/) to test this out
    on your local machine.
- Knowledge of Kubernetes
    Make sure you know how to work with KubeCTL and deploy
    applications to your cluster or use a KubeConfig.

## Running Locally

In order to perform development without waiting for a pipeline, we can
run this application locally. There is a Makefile which will make
everything easier to run.

1. Install Minikube

2. Run Minikube

```bash
$ minikube start

😄  minikube v1.25.2 on Darwin 12.3 (arm64)
✨  Using the podman (experimental) driver based on existing profile
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
E0321 11:05:07.616563   66007 cache.go:203] Error downloading kic artifacts:  not yet implemented, see issue #8426
🔄  Restarting existing podman container for "minikube" ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...E0321 11:05:13.251398   66007 start.go:126] Unable to get host IP: RoutableHostIPFromInside is currently only implemented for linux
▪ kubelet.housekeeping-interval=5m
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
💡  kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

3. Build the application

```bash
$ make build

go mod download
GOOS=darwin GOARCH=arm64 go build -o secreto-server .
chmod +x secreto-server
```

**Note:** You may need to change $GOOS and $GOARCH in the
Makefile before running, since your laptop may have a
different architecture.

4. Run the application in local mode

```bash
$ ./secreto-server -local

2022/03/20 16:18:30 KubeClient running with local configuration
2022/03/20 16:18:30 Starting server on the port 8080
```

5. Verify Server is running correctly

```bash
$ curl http://localhost:8080/api/secreto/version

{"version":1}
```

## File Structure

|  File   |     Purpose     |
|---------|-----------------|
| main.go | Start the application server. |
| router.go | Contains all the routes linked to a function in the middleware |
| models.go | All the structs the application will use in the application. |
| middleware.go | All the application function which grabs data from the Kube Client. For other things not related to Kube we can make another directory for. For example if we were using an SQLite DB for something, we can split middleware.go into kube.go and sqlite.go |

## Unit Tests

This application contains unit-test for making sure the application is
logically correct. The tests mocks the following items:

- Kubernetes Client
- Mux Http-Client

In order to run unittests, run the following command:

```bash
$ make test

go test -v ./...
?    gitlab.com/k2511/kube-secreto [no test files]
=== RUN   TestProcessSecrets
--- PASS: TestProcessSecrets (0.00s)
=== RUN   TestGetSecretsClient
--- PASS: TestGetSecretsClient (0.00s)
=== RUN   TestGetSecretDetailsClient
--- PASS: TestGetSecretDetailsClient (0.00s)
=== RUN   TestCreateSecretClient
--- PASS: TestCreateSecretClient (0.00s)
=== RUN   TestDeleteSecretClient
--- PASS: TestDeleteSecretClient (0.00s)
=== RUN   TestGetSecretsByNamespace
--- PASS: TestGetSecretsByNamespace (0.00s)
=== RUN   TestGetSecretDetails
--- PASS: TestGetSecretDetails (0.00s)
=== RUN   TestCreateSecret
--- PASS: TestCreateSecret (0.00s)
=== RUN   TestDeleteSecret
--- PASS: TestDeleteSecret (0.00s)
=== RUN   TestGetVersion
--- PASS: TestGetVersion (0.00s)
PASS
ok   gitlab.com/k2511/kube-secreto/internal/server/middleware 1.406s
?    gitlab.com/k2511/kube-secreto/internal/server/models [no test files]
?    gitlab.com/k2511/kube-secreto/internal/server/router [no test files]
```

## Design Explanation

There are both internal and external ways of deploying an application which
interacts with a Kubernetes Cluster.  

Internal is used if the application will live in Kubernetes and we will be accessing
it directly from the cluster. In this method, we deploy the application via Helm
and expose the API so that it can be accessed from anywhere. If this were to be done
some type of AUTHN/AUTHZ system should be put in place so that only those who should
access can.

External is used if running this as a local application which interfaces with a cluster
for which I want to use my kubeconfig to manage my secrets. The server should be deployed
in local mode. To enable local mode simply just pass the **-local** flag when running the
application.

### How it works

#### Secret Creation

Secrets are created by recieving a request containing:

- name
- payload

then we call the Kubernetes API using Client-Go and create a secret, setting the payload
as map[`shh`] = `payload`.

### Purpose of Application

The purpose of this application is mainly to teach others how to integrate the Kuberentes Client
into their application. This application can create, delete, list, and describe Kubernetes secrets via an API.

### Why Gorilla and not GoKit

Simplicity really. I built 2 separate microservices in different languages, and 1 accepts requests, the
other one sends requests when interacting with the UI. I didn't need anything more complex than that.

**Gorilla:** Package gorilla/mux implements a request router and dispatcher for matching incoming requests to their respective handler.

**GoKit:** Go kit is a programming toolkit for building microservices (or elegant monoliths) in Go. It solves common problems in distributed systems and application architecture so you can focus on delivering business value.

This [Quora Post](https://www.quora.com/What-are-the-differences-between-Gorilla-the-golang-web-toolkit-and-go-kit-kit#:~:text=Gorilla%20has%20been%20around%20for,and%20be%20found%20in%20turn.) provides some good content on Gorilla vs. GoKit.

---
