package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/k2511/kube-secreto/internal/server/middleware"
	"gitlab.com/k2511/kube-secreto/internal/server/router"
)

func main() {
	// Flag for running locally
	localFlag := flag.Bool("local", false, "Uses local Kubernetes configuration for accessing your cluster")
	flag.Parse()

	if *localFlag {
		log.Printf("KubeClient running with local configuration")
		middleware.InitLocal()
	} else {
		log.Printf("KubeClient running with in-cluster configuration")
		middleware.Init()
	}

	// Get port from env, or set to 8080 by default
	port := os.Getenv("SECRETO_PORT")
	if port == "" {
		port = "8080"
	}

	_, err := strconv.Atoi(port)
	if err != nil {
		log.Fatal(`"SECRETO_PORT" must be an integer`)
	}

	r := router.Router()
	log.Printf("Starting server on the port %s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), r))
}
