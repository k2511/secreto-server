package middleware

import (
	"context"
	"encoding/json"
	"flag"
	"net/http"
	"path/filepath"

	"github.com/gorilla/mux"
	"gitlab.com/k2511/kube-secreto/internal/server/models"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

var ClientSet kubernetes.Interface

const PayloadLabel = "shhh"
const AppLabel = "app"
const SecretoLabel = "secreto"
const Version = 1.0

// GetVersion returns the version of the Web-Server
func GetVersion(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")

	version := map[string]float64{"version": Version}
	json.NewEncoder(w).Encode(version)
}

// GetSecretsByNameSpace returns all secrets for a given namespace
// containing name, namespace, and createdtime
func GetSecretsByNameSpace(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// Post is requried since it's the same path used for creating a
	// secret
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, POST")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")

	params := mux.Vars(r)
	namespace := params["namespace"]

	secrets, err := getSecrets(namespace)
	if err != nil {
		json.NewEncoder(w).Encode(err.Error())
	}
	secretos := processSecrets(secrets)
	if err != nil {
		json.NewEncoder(w).Encode(err.Error())
	}

	json.NewEncoder(w).Encode(secretos)
}

// getSecrets uses the Kube Client to obtain secrets from cluster
func getSecrets(namespace string) ([]v1.Secret, error) {
	secrets, err := ClientSet.CoreV1().Secrets(namespace).List(context.TODO(), metav1.ListOptions{})

	if err != nil {
		return nil, err
	}

	return secrets.Items, nil
}

// processSecrets converts secrets into secretos obtaining only the data we need
func processSecrets(secrets []v1.Secret) []*models.Secreto {
	var secretos []*models.Secreto

	for _, secret := range secrets {

		secreto := &models.Secreto{
			Name:              secret.GetName(),
			NameSpace:         secret.GetNamespace(),
			CreationTimestamp: secret.GetCreationTimestamp().String(),
			Payload:           string(secret.Data[PayloadLabel]),
		}

		secretos = append(secretos, secreto)
	}

	return secretos
}

// GetSecretDetails returns a given secret with detailed information
func GetSecretDetails(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// Delete is required here since it contains the same path used for
	// a Delete, we can also check the methods in one function and
	// perform the action for modularity
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")

	params := mux.Vars(r)
	name := params["name"]
	namespace := params["namespace"]

	secret, err := getSecretDetails(namespace, name)
	if err != nil {
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	secreto := models.Secreto{
		Name:              secret.Name,
		NameSpace:         secret.Namespace,
		CreationTimestamp: secret.CreationTimestamp.String(),
		Payload:           string(secret.Data[PayloadLabel]), //StringData[PayloadLabel],
	}

	json.NewEncoder(w).Encode(secreto)
}

// getSecretDetails uses the Kube Client to obtain a secret within a given namespace
func getSecretDetails(namespace string, name string) (*v1.Secret, error) {
	secret, err := ClientSet.CoreV1().Secrets(namespace).Get(context.TODO(), name, metav1.GetOptions{})

	if err != nil {
		return nil, err
	}

	return secret, nil
}

// Create Secret creates a secret with data passed in the request
func CreateSecret(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	params := mux.Vars(r)
	namespace := params["namespace"]

	// Grab the json data sent in the request
	var secretRequest models.SecretCreateRequestData
	err := json.NewDecoder(r.Body).Decode(&secretRequest)
	if err != nil {
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	secreto, err := createSecret(namespace, secretRequest.Name, secretRequest.Payload)
	if err != nil {
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	result := map[string]models.Secreto{"Secret Created Successfully": *secreto}
	json.NewEncoder(w).Encode(result)
}

// createSecret uses the Kubernetes API to generate a secret from passed data
func createSecret(namespace string, name string, data string) (*models.Secreto, error) {

	// Add a label to the secret letting us know
	// it was generated by this application
	secretoLabels := map[string]string{}
	secretoLabels[AppLabel] = SecretoLabel

	// Setup Secret Name and Namespace
	objMeta := metav1.ObjectMeta{
		Name:      name,
		Namespace: namespace,
		Labels:    secretoLabels,
	}

	// Setup Actual Secret Data
	secretData := map[string]string{}
	secretData[PayloadLabel] = data

	// Setup Actual Secret Data in bytes
	secretDataBytes := map[string][]byte{}
	secretDataBytes[PayloadLabel] = []byte(data)

	// Setup Complete Secret
	secret := &v1.Secret{
		ObjectMeta: objMeta,
		StringData: secretData,
		Data:       secretDataBytes,
	}

	// Create the Secret
	generatedSecret, err := ClientSet.CoreV1().Secrets(namespace).Create(context.TODO(), secret, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	// Create object to return
	secreto := models.Secreto{
		Name:              generatedSecret.Name,
		NameSpace:         generatedSecret.Namespace,
		Payload:           string(generatedSecret.Data[PayloadLabel]),
		CreationTimestamp: generatedSecret.CreationTimestamp.String(),
	}

	return &secreto, nil
}

// DeleteSecret deletes a secret by a given name and namespace
func DeleteSecret(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")

	params := mux.Vars(r)
	name := params["name"]
	namespace := params["namespace"]

	err := deleteSecret(name, namespace)
	if err != nil {
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	result := map[string]string{"Secret Deleted Successfully": name}
	json.NewEncoder(w).Encode(result)
}

// deleteSecret use the Kubernetes API to delete a Secret
func deleteSecret(name string, namespace string) error {
	err := ClientSet.CoreV1().Secrets(namespace).Delete(context.TODO(), name, metav1.DeleteOptions{})
	if err != nil {
		return err
	}

	return nil
}

// Init creates the In-Cluster configuration to use the application
// within the cluster
func Init() {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	// set the global variable to use within this class
	ClientSet = clientset
}

// InitLocal creates the local configuration to use the application
// locally
func InitLocal() {
	var kubeconfig *string

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	// set the global variable to use within this class
	ClientSet = clientset
}
