package middleware

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/k2511/kube-secreto/internal/server/models"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

// Utlity function to setup the secrets used for testing
func setupSecrets() (names []string, namespaces []string,
	dates []metav1.Time, payload map[string]string, secrets []v1.Secret) {

	// These are returned in order to verify test data in other functions
	names = []string{"yeet", "yooo", "yeah"}
	namespaces = []string{"default", "default", "kube-system"}
	dates = []metav1.Time{metav1.Date(2020, time.November, 1, 2, 0, 0, 0, time.UTC),
		metav1.Date(2021, time.November, 3, 4, 0, 0, 0, time.UTC),
		metav1.Date(2022, time.November, 10, 15, 0, 0, 0, time.UTC),
	}
	payload = make(map[string]string)
	payload[PayloadLabel] = "WOWW"

	payloadRaw := make(map[string][]byte)
	payloadRaw[PayloadLabel] = []byte("WOWW")

	secretOne := v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:              names[0],
			Namespace:         namespaces[0],
			CreationTimestamp: dates[0],
		},
		StringData: payload,
		Data:       payloadRaw,
	}
	secretTwo := v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:              names[1],
			Namespace:         namespaces[1],
			CreationTimestamp: dates[1],
		},
		StringData: payload,
		Data:       payloadRaw,
	}
	secretThree := v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:              names[2],
			Namespace:         namespaces[2],
			CreationTimestamp: dates[2],
		},
		StringData: payload,
		Data:       payloadRaw,
	}
	secrets = []v1.Secret{secretOne, secretTwo, secretThree}

	return names, namespaces, dates, payload, secrets
}

// TestProcessSecrets calls middleware.processSecrets with
// a valid list of secrets
func TestProcessSecrets(t *testing.T) {
	// Run the test function
	names, namespaces, dates, payload, secrets := setupSecrets()
	result := processSecrets(secrets)
	secretData := payload[PayloadLabel]

	// Setup Expected
	SecretoOne := models.Secreto{Name: names[0], NameSpace: namespaces[0], CreationTimestamp: dates[0].Time.String(), Payload: secretData}
	SecretoTwo := models.Secreto{Name: names[1], NameSpace: namespaces[1], CreationTimestamp: dates[1].Time.String(), Payload: secretData}
	SecretoThree := models.Secreto{Name: names[2], NameSpace: namespaces[2], CreationTimestamp: dates[2].Time.String(), Payload: secretData}
	expected := []models.Secreto{SecretoOne, SecretoTwo, SecretoThree}

	if reflect.DeepEqual(expected, result) {
		t.Errorf("expected did not match result, expected: %v, got: %v.", expected, result)
	}
}

// TestGetSecretsClient calls middleware.getSecrets which uses
// the Kube Client to obtain all secrets
func TestGetSecretsClient(t *testing.T) {
	// Use the FakeClient
	ClientSet = fake.NewSimpleClientset()
	_, namespaces, _, _, secrets := setupSecrets()

	// Create Secret in Default namespace
	secret, err := ClientSet.CoreV1().Secrets(namespaces[0]).Create(context.TODO(), &secrets[0], metav1.CreateOptions{})
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	// Try Getting Secrets in default Namespace
	result, err := getSecrets("default")
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}
	expected := []*v1.Secret{secret}
	if reflect.DeepEqual(expected, result) {
		t.Errorf("expected did not match result, expected: %v, got: %v.", expected, result)
	}

	// Create a Secret in Kube-System NameSpace
	secretKube, err := ClientSet.CoreV1().Secrets(namespaces[2]).Create(context.TODO(), &secrets[2], metav1.CreateOptions{})
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	// Try kube-system Namespace
	result, err = getSecrets("kube-system")
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}
	expected = []*v1.Secret{secretKube}
	if reflect.DeepEqual(expected, result) {
		t.Errorf("expected did not match result, expected: %v, got: %v.", expected, result)
	}
}

// TestGetSecretDetailsClient calls middleware.getSecretDetails which uses
// the Kube Client to obtain secrets along with the payload
func TestGetSecretDetailsClient(t *testing.T) {
	// Use the FakeClient
	ClientSet = fake.NewSimpleClientset()
	names, namespaces, _, _, secrets := setupSecrets()

	// Create Secret in Default namespace
	_, err := ClientSet.CoreV1().Secrets(namespaces[0]).Create(context.TODO(), &secrets[0], metav1.CreateOptions{})
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	// Try Getting Secrets in default Namespace
	result, err := getSecretDetails(namespaces[0], names[0])
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}
	expected := secrets[0]
	if reflect.DeepEqual(expected, result) {
		t.Errorf("expected did not match result, expected: %v, got: %v.", expected, result)
	}
}

// TestGetSecretCreateClient calls middleware.createSecret which uses
// the Kube Client to create a secret
func TestCreateSecretClient(t *testing.T) {
	// Use the FakeClient
	ClientSet = fake.NewSimpleClientset()
	names, namespaces, _, payload, _ := setupSecrets()
	secretData := payload[PayloadLabel]

	// Create Secret in Default namespace
	result, err := createSecret(namespaces[0], names[0], "WOWWWWWW")
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	expected := models.Secreto{
		Name:      names[0],
		NameSpace: namespaces[0],
		Payload:   secretData,
	}

	if reflect.DeepEqual(expected, result) {
		t.Errorf("expected did not match result, expected: %v, got: %v.", expected, result)
	}
}

// TestDeleteSecretClient calls middleware.deleteSecret which uses
// the Kube Client to delete a secret
func TestDeleteSecretClient(t *testing.T) {
	// Use the FakeClient
	ClientSet = fake.NewSimpleClientset()
	names, namespaces, _, _, secrets := setupSecrets()

	// Create Secret in Default namespace
	_, err := ClientSet.CoreV1().Secrets(namespaces[0]).Create(context.TODO(), &secrets[0], metav1.CreateOptions{})
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	// Try Getting Secrets in default Namespace
	result, err := getSecretDetails(namespaces[0], names[0])
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}
	expected := secrets[0]
	if reflect.DeepEqual(expected, result) {
		t.Errorf("expected did not match result, expected: %v, got: %v.", expected, result)
	}

	// Delete Secret
	err = deleteSecret(names[0], namespaces[0])
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	// If the secret exists then fail, else pass
	_, err = getSecretDetails(namespaces[0], names[0])
	if err == nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}
}

// TestGetSecretsByNamespace mocks a request in order to
// obtain secrets for a given namespace
func TestGetSecretsByNamespace(t *testing.T) {

	ClientSet = fake.NewSimpleClientset()
	names, namespaces, _, payload, _ := setupSecrets()
	secretData := payload[PayloadLabel]

	// Create Secret in Default namespace
	secret, err := createSecret(namespaces[0], names[0], secretData)
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}
	expected, err := json.Marshal([]models.Secreto{*secret})
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	r, _ := http.NewRequest("GET", "/api/secreto/default", nil)
	w := httptest.NewRecorder()

	// Fake passing the URL Variables
	vars := map[string]string{
		"namespace": namespaces[0],
	}
	r = mux.SetURLVars(r, vars)

	// Perform the function
	GetSecretsByNameSpace(w, r)

	result := w.Body.String()

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(expected)+"\n", result)
}

// TestGetSecretDetails mocks a request in order to
// obtain details on a secret
func TestGetSecretDetails(t *testing.T) {
	ClientSet = fake.NewSimpleClientset()
	names, namespaces, _, payload, _ := setupSecrets()
	secretData := payload[PayloadLabel]

	// Create Secret in Default namespace
	secret, err := createSecret(namespaces[0], names[0], secretData)
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	expected, err := json.Marshal(secret)
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	r, _ := http.NewRequest("GET", "/api/secreto/default/yeet", nil)
	w := httptest.NewRecorder()

	// Fake passing the URL Variables
	vars := map[string]string{
		"namespace": namespaces[0],
		"name":      names[0],
	}
	r = mux.SetURLVars(r, vars)

	// Perform the function
	GetSecretDetails(w, r)

	result := w.Body.String()

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(expected)+"\n", result)
}

// TestCreateSecrets mocks a request in order to
// create a secret
func TestCreateSecret(t *testing.T) {
	ClientSet = fake.NewSimpleClientset()
	names, namespaces, _, payload, _ := setupSecrets()
	secretData := payload[PayloadLabel]

	// Setup the Request
	requestBody := []byte(fmt.Sprintf(`{"name":"%s", "payload":"%s"}`, names[0], secretData))
	r, _ := http.NewRequest("POST", "/api/secreto/default", bytes.NewBuffer(requestBody))
	w := httptest.NewRecorder()

	// Fake passing the URL Variables
	vars := map[string]string{
		"namespace": namespaces[0],
	}
	r = mux.SetURLVars(r, vars)

	// Perform the function
	CreateSecret(w, r)

	expectedJSON := map[string]*models.Secreto{"Secret Created Successfully": &models.Secreto{
		Name:      names[0],
		NameSpace: namespaces[0],
		// date is hardcoded since it's generated by the fakeclient
		CreationTimestamp: "0001-01-01 00:00:00 +0000 UTC",
		Payload:           secretData,
	}}
	expected, _ := json.Marshal(expectedJSON)
	result := w.Body.String()

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(expected)+"\n", result)
}

// TestDeleteSecret mocks a request in order to
// obtain details on a secret
func TestDeleteSecret(t *testing.T) {
	ClientSet = fake.NewSimpleClientset()
	names, namespaces, _, payload, _ := setupSecrets()
	secretData := payload[PayloadLabel]

	// Create Secret in Default namespace
	_, err := createSecret(namespaces[0], names[0], secretData)
	if err != nil {
		t.Errorf("Unexpected Error: '%v'", err)
	}

	r, _ := http.NewRequest("DELETE", "/api/secreto/default/yeet", nil)
	w := httptest.NewRecorder()

	// Fake passing the URL Variables
	vars := map[string]string{
		"namespace": namespaces[0],
		"name":      names[0],
	}
	r = mux.SetURLVars(r, vars)

	// Perform the function
	DeleteSecret(w, r)

	expectedJSON := map[string]string{"Secret Deleted Successfully": names[0]}
	expected, _ := json.Marshal(expectedJSON)
	result := w.Body.String()

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(expected)+"\n", result)
}

// TestGetVersion calls middleware.GetVersion which returns
// the Application Version
func TestGetVersion(t *testing.T) {
	ClientSet = fake.NewSimpleClientset()
	r, _ := http.NewRequest("GET", "/api/secreto/version", nil)
	w := httptest.NewRecorder()

	// Perform the function
	GetVersion(w, r)

	expectedJSON := map[string]float64{"version": Version}
	expected, _ := json.Marshal(expectedJSON)
	result := w.Body.String()

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(expected)+"\n", result)
}
