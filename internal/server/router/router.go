package router

import (
	"gitlab.com/k2511/kube-secreto/internal/server/middleware"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/api/secreto/version", middleware.GetVersion).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/secreto", middleware.GetSecretsByNameSpace).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/secreto/{namespace}", middleware.GetSecretsByNameSpace).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/secreto/{namespace}/{name}", middleware.GetSecretDetails).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/secreto/{namespace}", middleware.CreateSecret).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/secreto/{namespace}/{name}", middleware.DeleteSecret).Methods("DELETE", "OPTIONS")
	return router
}
