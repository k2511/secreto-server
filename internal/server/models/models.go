package models

// SecretCreateRequestData contains the data obtained in
// order to create a secret
type SecretCreateRequestData struct {
	Name    string `json:"name"`
	Payload string `json:"payload"`
}

// Secreto is used for displaying secret in detail
type Secreto struct {
	Name              string `json:"name"`
	NameSpace         string `json:"namespace"`
	CreationTimestamp string `json:"date"`
	Payload           string `json:"payload"`
}
