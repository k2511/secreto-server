GO = go
GOOS = darwin
GOARCH = arm64
DOCKER = docker
#REGISTRY = INSERT LOCAL REGISTRY
TAG = latest

build:
	${GO} mod download
	GOOS=${GOOS} GOARCH=${GOARCH} ${GO} build -o secreto-server .
	chmod +x secreto-server

test:
	${GO} test -v ./...

docker-build:
	${DOCKER} 

docker-push:
	${DOCKER}

clean:
	rm -rf secreto-server
